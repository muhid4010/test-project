<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1251"/>
    <title>Кредитные отчеты</title>
    <style type="text/css">
    table{ border-collapse:collapse; width:100%; }
    table td, th{ border:1px solid #d0d0d0; }
  </style>
    <style type="text/css">#main_block table {font-size:9px;} .header-color {background-color:#8EB2E2;} .mip-color {background-color:#e3fbf7;} .procent25_class {width:25%;}</style>
  </head>
  <body style="height:99%;width:99%;">
    <div id="main_block">
      <div id="wrong_rep_type"/>
      <div id="headerMKB">
        <span>
          <div>
            <table style="width:100%;text-align:center;">
              <tr style="color:#17365d;font-size:20px;">
                <td style="width:50%;">&quot;КРЕДИТ-АХБОРОТ ТАХЛИЛИЙ МАРКАЗИ&quot; КРЕДИТ БЮРОСИ</td>
                <td style="width:50%;">CREDIT BUREAU &quot;CREDIT INFORMATION-ANALYTICAL CENTRE&quot;</td>
              </tr>
              <tr>
                <td colspan="2" style="border-top:3px solid #17365d;font-size:12px;border-bottom:3px solid #17365d;">Pochta manzili: 100027, Тоshkent sh., Qoratosh к., 1 uy. Banklararo telekommunikasion tarmoqdagi elektron manzili (lotus notes): &quot;katm1/Uzb@Banks&quot;; telefon:(99871) 238 69 71; 238 69 31</td>
              </tr>
            </table>
            <br/>
            <p>
              <b>Внимание:</b> Предоставление и использование кредитной информации регулируется Законом Республики Узбекистан &quot;Об обмене кредитной информацией&quot; № 301 от 04.10.2011 года.
    </p>
          </div>
        </span>
      </div>
      <div id="content_general">
        <table not_sort="true" id="table_general" class="table_personal_class">
          <tr>
            <td align="center" class="header-color">
              <h2>ИДЕНТИФИКАЦИЯ СУБЪЕКТА КРЕДИТНОЙ ИНФОРМАЦИИ</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table not_sort="true" class="table_personal_class">
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>№</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>ТИП ИНФОРМАЦИИ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>ИНФОРМАЦИЯ</b>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>1</b>
                  </td>
                  <td id="info_type1" style="text-align:left">
                    <b>Наименование заёмщика</b>
                  </td>
                  <td id="info_data1" style="text-align:left">
                    <span>Xalbayev Muxiddin Abdumalik ugli</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>2</b>
                  </td>
                  <td id="info_type2" style="text-align:left">
                    <b/>
                    <span>Дата рождения</span>
                  </td>
                  <td id="info_data2" style="text-align:left">
                    <span>1991-06-01</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>3</b>
                  </td>
                  <td id="info_type3" style="text-align:left">
                    <b>Резидентность</b>
                  </td>
                  <td id="info_data3" style="text-align:left">
                    <span>Резидент</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>4</b>
                  </td>
                  <td id="info_type4" style="text-align:left">
                    <b/>
                    <span>Пол</span>
                  </td>
                  <td id="info_data4" style="text-align:left">
                    <span>М</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>5</b>
                  </td>
                  <td id="info_type5" style="text-align:left">
                    <b/>
                    <span>Адрес по прописке</span>
                  </td>
                  <td id="info_data5" style="text-align:left">
                    <span>Jizzax viloyati Baxmal tumani IIB</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>6</b>
                  </td>
                  <td id="info_type6" style="text-align:left">
                    <b>ПИНФЛ</b>
                  </td>
                  <td id="info_data6" style="text-align:left">
                    <span>12345678912345</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>7</b>
                  </td>
                  <td id="info_type7" style="text-align:left">
                    <b>ИНН</b>
                  </td>
                  <td id="info_data7" style="text-align:left">
                    <span>536330023</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>8</b>
                  </td>
                  <td id="info_type8" style="text-align:left">
                    <b/>
                    <span>Тип документа</span>
                  </td>
                  <td id="info_data8" style="text-align:left">
                    <span>Биометрический паспорт гражданина Республики Узбекистан</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>9</b>
                  </td>
                  <td id="info_type9" style="text-align:left">
                    <b/>
                    <span>Данные документа</span>
                  </td>
                  <td id="info_data9" style="text-align:left">
                    <span>AB 4276354 от 16 июня 2016</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>10</b>
                  </td>
                  <td id="info_type10" style="text-align:left">
                    <b>Пользователь кредитного отчёта</b>
                  </td>
                  <td id="info_data10" style="text-align:left">
                    <span>ТОШКЕНТ Ш., &quot;ТУРОНБАНК&quot; АТ БАНКИНИНГ ЮНУСОБОД ФИЛИАЛИ (оффлайн)</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>11</b>
                  </td>
                  <td id="info_type11" style="text-align:left">
                    <b>Кредитная заявка</b>
                  </td>
                  <td id="info_data11" style="text-align:left">
                    <span>4010 от 20 июля 2020</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>12</b>
                  </td>
                  <td id="info_type12" style="text-align:left">
                    <b>№ запроса пользователя</b>
                  </td>
                  <td id="info_data12" style="text-align:left">
                    <span>0112020253308732</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding-top: 15px;">
           </td>
          </tr>
          <tr>
            <td align="center" class="header-color">
              <h2>СВЕДЕНИЯ О НАЛИЧИИ ВОЗМОЖНОСТИ ПОЛУЧЕНИЯ КРЕДИТНЫХ ОТЧЕТОВ ПО ЗАЁМЩИКУ</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table not_sort="true" class="report_table_class">
                <tr class="center_class">
                  <td class="procent25_class header-color">
                    <b>Тип кредитного отчета:</b>
                  </td>
                  <td class="procent25_class">
                    <b>&quot;Идентификация субъекта кредитной информации&quot;</b>
                  </td>
                  <td class="procent25_class">
                    <b>&quot;Информация по кредитам&quot;</b>
                  </td>
                  <td class="procent25_class">
                    <b>&quot;Статистика запросов&quot;</b>
                  </td>
                  <td class="procent25_class">
                    <b>&quot;Скоринговый балл&quot;</b>
                  </td>
                </tr>
                <tr class="center_class">
                  <td class="header-color">
                    <b>Наличие</b>
                  </td>
                  <td id="date_update_b">
                    <span>Да</span>
                  </td>
                  <td id="date_update_b_1">
                    <span>Да</span>
                  </td>
                  <td id="date_update_b_2">
                    <span>Да</span>
                  </td>
                  <td id="date_update_b_3">
                    <span>Да</span>
                  </td>
                </tr>
                <tr class="center_class">
                  <td class="header-color">
                    <b>Дата обновления</b>
                  </td>
                  <td id="date_update_d">
                    <span>09.09.2020</span>
                  </td>
                  <td id="date_update_d_1">
                    <span>20.07.2020</span>
                  </td>
                  <td id="date_update_d_2">
                    <span>09.09.2020</span>
                  </td>
                  <td id="date_update_d_3">
                    <span>09.09.2020</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table not_sort="true" id="table_request_inf" class="report_table_class">
          <tr>
            <td style="padding-top: 15px;">
		   </td>
          </tr>
          <tr>
            <td align="center" class="header-color">
              <h2>ИНФОРМАЦИЯ ОБ ОБРАЩЕНИЯХ СУБЪЕКТА</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table not_sort="true" class="report_table_class">
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>№</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>ТИП ОРГАНИЗАЦИИ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>КОЛИЧЕСТВО ЗАЯВОК</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>КОЛИЧЕСТВО ОТКЛОНЕНИЙ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>КОЛИЧЕСТВО ДОГОВОРОВ</b>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>1</b>
                  </td>
                  <td id="org_type1" style="text-align:center">
                    <b/>
                    <span>Коммерческий банк</span>
                  </td>
                  <td id="claims_qty1" style="text-align:center">
                    <span>9</span>
                  </td>
                  <td id="dec_qty1" style="text-align:center">
                    <span>4</span>
                  </td>
                  <td id="cont_qty1" style="text-align:center">
                    <span>2</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>2</b>
                  </td>
                  <td id="org_type2" style="text-align:center">
                    <b/>
                    <span>Лизинговая компания</span>
                  </td>
                  <td id="claims_qty2" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="dec_qty2" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="cont_qty2" style="text-align:center">
                    <span>0</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>3</b>
                  </td>
                  <td id="org_type3" style="text-align:center">
                    <b/>
                    <span>Ломбард</span>
                  </td>
                  <td id="claims_qty3" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="dec_qty3" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="cont_qty3" style="text-align:center">
                    <span>0</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>4</b>
                  </td>
                  <td id="org_type4" style="text-align:center">
                    <b/>
                    <span>Микрокредитная организация</span>
                  </td>
                  <td id="claims_qty4" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="dec_qty4" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="cont_qty4" style="text-align:center">
                    <span>0</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>5</b>
                  </td>
                  <td id="org_type5" style="text-align:center">
                    <b/>
                    <span>Ритейл</span>
                  </td>
                  <td id="claims_qty5" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="dec_qty5" style="text-align:center">
                    <span>0</span>
                  </td>
                  <td id="cont_qty5" style="text-align:center">
                    <span>0</span>
                  </td>
                </tr>
                <tr>
                  <td class="tg-amwm" colspan="2" style="text-align:center">
                    <b>Итого</b>
                  </td>
                  <td id="claims_qty6" style="text-align:center">
                    <b/>
                    <span>9</span>
                  </td>
                  <td id="dec_qty6" style="text-align:center">
                    <b/>
                    <span>4</span>
                  </td>
                  <td id="cont_qty6" style="text-align:center">
                    <b/>
                    <span>2</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding-top: 15px;">
		   </td>
          </tr>
        </table>
        <table not_sort="true" id="table_delays_info" class="table_delays_class_table">
          <tr>
            <td style="padding-top: 15px;">
           </td>
          </tr>
          <tr>
            <td align="center" class="header-color">
              <h2>ИНФОРМАЦИЯ О НАЛИЧИЯХ ЗАДОЛЖЕННОСТИ СУБЪЕКТА</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table not_sort="true" class="table_delays_class">
                <tr>
                  <td class="tg-amwm" style="text-align:center">
                    <b>№</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>НАИМЕНОВАНИЕ ОРГАНИЗАЦИИ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>ВАЛЮТА КОНТРАКТА</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>СУММА НЕПОГАШЕННЫХ ОБЯЗАТЕЛЬСТВ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>НЕПОГАШЕННЫЕ ПРОСРОЧКИ</b>
                  </td>
                  <td class="tg-amwm" style="text-align:center">
                    <b>ДАТА ОБНОВЛЕНИЯ</b>
                  </td>
                </tr>
                <tr>
                  <td class="org_num1" style="text-align:center">
                    <b/>
                    <span>1</span>
                  </td>
                  <td id="org_name1" style="text-align:left">
                    <b/>
                    <span>ТОШКЕНТ Ш., &quot;ТУРОНБАНК&quot; АТ БАНКИНИНГ ЮНУСОБОД ФИЛИАЛИ</span>
                  </td>
                  <td id="currency1" style="text-align:center">
                    <span>UZS</span>
                  </td>
                  <td id="cur_all_debts1" style="text-align:center">
                    <span>                234 176 068.49</span>
                  </td>
                  <td id="cur_delays1" style="text-align:center">
                    <span>                  4 676 068.49</span>
                  </td>
                  <td id="last_updated1" style="text-align:center">
                    <span>2020-09-08</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding-top: 15px;">
                               </td>
          </tr>
        </table>
        <table not_sort="true" id="table_average_pay_info" class="report_table_class">
          <tr>
            <td style="padding-top: 15px;"/>
          </tr>
          <tr>
            <td align="center" class="header-color">
              <h2>СРЕДНЕМЕСЯЧНЫЙ ПЛАТЁЖ</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table not_sort="true" class="info_average_pay_exists">
                <tr class="center_class">
                  <td id="empty" style="text-align:center">              5 060 038 сум.<b/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding-top: 15px;"/>
          </tr>
        </table>
      </div>
      <div class="hash_rep_name"/>
      <div class="hash_rep_val"/>
    </div>
  </body>
</html>