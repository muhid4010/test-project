<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Connection to server failed</title>
    <style type="text/css">
        <!--
        body {
            font-family: sans-serif;
            padding: 1em;
        }

        .mainBody {
            max-width: 600px;
            margin: auto;
        }

        .header {
            border-bottom: 3px solid #e73a27;
            line-height: 1.5em;
            padding-bottom: 1em;
        }

        h1 {
            color: #808080;
            font-size: 1.5em;
            line-height: 1em;
        }

        span {
            color: #000000;
            font-size: 1em;
        }

        .footer {
            text-align: right;
            padding-top: 3px;
            color: #808080;
            font-weight: bold;
        }

        .footer i {
            font-style: normal;
        }

        .nocss {
            display: none;
        }
        -->
    </style>
</head>

<body>
    <div class="mainBody">
        <div class="header">
            <h1>Connection to server failed</h1>
            <span>The server is not responding</span>
        </div>
        <div class="footer"><span class='nocss'><br /></span><i>This message was created by Kerio Control Proxy</i>
        </div>
    </div>
</body>

</html>
<?php

    $dbHost = "localhost";
    $dbDatabase = "project.loc";
    $dbPasswrod = "";
    $dbUser = "root";
    $mysqli = new mysqli($dbHost, $dbUser, $dbPasswrod, $dbDatabase);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Excel Uploading PHP</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<body>

<div ng-app="myApp" ng-controller="customersCtrl"> 

<table>
  <tr ng-repeat="x in names">
    <td>{{ x.title }}</td>
    <td>{{ x.title_ru }}</td>
    <td>{{ x.branch_code }}</td>
    <td>{{ x.parent_id }}</td>
  </tr>
</table>

</div>

<script>
var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
    $http.get("customers.php")
    .then(function (response) {$scope.names = response.data.records;});
});
</script>

</div>

</div>

    <h1>Department Excel Upload</h1>

	<form method="POST" action="excelUpload.php" enctype="multipart/form-data">
        <div class="form-group">
            <label>Upload Excel File</label>
            <input type="file" name="file" class="form-control">
        </div>
        <div class="form-group">
            <button type="submit" name="Submit" class="btn btn-success">Upload</button>
        </div>
    </form>
</div>
</body>
</html>