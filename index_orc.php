<?php
$db= "(DESCRIPTION =
(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.1.6)(PORT = 1521))
(CONNECT_DATA =
(SERVER = DEDICATED)
(SERVICE_NAME = IABS)
)
)";
$conn = oci_connect('ibs', 'q2w3e4r', $db, 'AL32UTF8');
if (!$conn)
{
$e = oci_error();
trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
/*else
{
die("connected");
}*/

$sql = "select a.* from dwh_loan_card a
where 1=1
and a.region_code = '08'
and a.OPEN_DATE > '01-MAY-17'
and a.OPEN_DATE < '31-MAY-20'
order by a.loan_id desc";

$stid = oci_parse($conn, $sql);
oci_execute($stid);

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<table class="table-bordered">
    <thead>
    <tr>
        <th>filial</th>
        <th><i class="fa fa-user-plus"></i> client code</th>
        <th><i class="fa fa-text-height"></i> client _name</th>
        <th><i class="fa fa-tag"></i> # ariza</th>
        <th><i class="fa fa-hourglass-start"></i> summa</th>
        <th><i class="fa fa-paperclip"></i> data </th>
        <th><i class="fa fa-clock-o"></i> credit type</th>
        <th><i class="fa fa-trash-o"></i>status</th>
    </tr>
    </thead>
    <tbody>
    <?php

    while (oci_fetch($stid)) { ?>

        <tr>
            <td>00982</td>
            <td><?php echo oci_result($stid, 'CLIENT_CODE'); ?></td>
            <td><?php echo oci_result($stid, 'CLIENT_NAME'); ?></td>
            <td><?php echo oci_result($stid, 'COMMITTEE_NUMBER'); ?></td>
            <td><?php echo number_format(oci_result($stid, 'SUMM_LOAN')/100, 2, '.', ','); ?></td>
            <td><?php echo oci_result($stid, 'OPEN_DATE'); ?></td>
            <td><?php echo oci_result($stid, 'LN_TYPE_NAME'); ?></td>
            <td><?php echo oci_result($stid, 'LN_STATUS_NAME'); ?></td>
        </tr>


    <?php } ?>

    </tbody>
</table>
<style>
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border: 1px solid #ddd;
        padding: 8px;
    }
</style>
</body>
</html>
