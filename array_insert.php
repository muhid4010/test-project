<?php
$dbHost = "localhost";
    $dbDatabase = "project.loc1";
    $dbPasswrod = "";
    $dbUser = "root";
    $mysqli = new mysqli($dbHost, $dbUser, $dbPasswrod, $dbDatabase);


//read the json file contents
$studentjsondata = file_get_contents('students.json');

//convert json object to php associative array

$json_data = '{"html":{
    "studentId": "ST0056",
    "personal": {
        "name": "John Muksid",
        "age": "29",
        "address": {
            "streetaddress": "5 14th Street",
            "city": "New Tashkent",
            "state": "NY",
            "postalcode": "125489"
        }
    }
}}';

$json39 = '{"report":{
  "incomes":{
    "INCOME":[{
      "ORG_INN":206870064,"INCOME_SUMMA":762080,"NUM":1,"PERIOD":"2019-09","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":703120,"NUM":2,"PERIOD":"2019-10","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":703120,"NUM":3,"PERIOD":"2019-11","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":703120,"NUM":4,"PERIOD":"2019-12","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":703120,"NUM":5,"PERIOD":"2020-01","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":1525920,"NUM":6,"PERIOD":"2020-02","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":1525920,"NUM":7,"PERIOD":"2020-03","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":752400,"NUM":8,"PERIOD":"2020-04","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":1823360,"NUM":9,"PERIOD":"2020-05","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":346720,"NUM":10,"PERIOD":"2020-06","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"},{
      "ORG_INN":206870064,"INCOME_SUMMA":116160,"NUM":11,"PERIOD":"2020-07","ORGNAME":"БАХМАЛ ТУМАНИ 39-МАКТАБ"}]},
  "incomes_period":{"incomes_period_begin":"2019-09-16","incomes_period_end":"2020-09-16","incomes_all_summa":9665040},
  "sysinfo":{"date":"2020-09-16 12:19:18","bank":"\"ТУРОНБАНК\" АТ БАНКИ","id_demand":"0112020260384466","user_id":"оффлайн","claim_id":1090113,"report_name":"Сведения о доходах по ИНПС XML","declaration":"Внимание! Предоставление и использование кредитной информации регулируется Законом Республики Узбекистан \"Об обмене кредитной информацией\" № 301 от 04.10.2011 года.","branch":"01144","claim_date":"2020-09-15","report_code":"025"},"presence_reports":{"presence_report":[{"num":2,"report_name":"Скоринг КИАЦ","presence":"Да"},{"num":1,"report_name":"Информация по кредитам","presence":"Да"}]},"client":{"pinfl":31710671620037,"address":"Jizzax viloyati Baxmal tumani Sovuqbuloq qish","document_number":"0339094","gender":"М","document_type_id":6,"birth_date":"1967-10-17","name":"Zulfanov Abdumalik Xolboyevich","inn":504652952,"document_date":"2017-10-30","resident":"Резидент","document_type":"Биометрический паспорт гражданина Республики Узбекистан","document_serial":"AA"},"notifications":""}}
';


$base_64 = 'eyJyZXBvcnQiOnsiaW5jb21lcyI6eyJJTkNPTUUiOlt7Ik9SR19JTk4iOjIwMzc1MjM3OSwiSU5DT01FX1NVTU1BIjo1NDQxNDgwLCJOVU0iOjEsIlBFUklPRCI6IjIwMTktMDgiLCJPUkdOQU1FIjoi0KLQo9Cg0J7QnSDQkdCQ0J3QmiJ9LHsiT1JHX0lOTiI6MjAzNzUyMzc5LCJJTkNPTUVfU1VNTUEiOjEuMDEyOTE3ODRFNywiTlVNIjoyLCJQRVJJT0QiOiIyMDE5LTEwIiwiT1JHTkFNRSI6ItCi0KPQoNCe0J0g0JHQkNCd0JoifSx7Ik9SR19JTk4iOjIwMzc1MjM3OSwiSU5DT01FX1NVTU1BIjo4NzI4NDUuNiwiTlVNIjozLCJQRVJJT0QiOiIyMDE5LTEyIiwiT1JHTkFNRSI6ItCi0KPQoNCe0J0g0JHQkNCd0JoifSx7Ik9SR19JTk4iOjIwMTA1NTEwOCwiSU5DT01FX1NVTU1BIjoyLjcxNTM0MDMyRTcsIk5VTSI6NCwiUEVSSU9EIjoiMjAyMC0wMiIsIk9SR05BTUUiOiLQkNCa0JEg0KLRg9GA0L7QvdCx0LDQvdC6In0seyJPUkdfSU5OIjoyMDEwNTUxMDgsIklOQ09NRV9TVU1NQSI6ODc1MTIxMi44LCJOVU0iOjUsIlBFUklPRCI6IjIwMjAtMDMiLCJPUkdOQU1FIjoi0JDQmtCRINCi0YPRgNC+0L3QsdCw0L3QuiJ9LHsiT1JHX0lOTiI6MjAxMDU1MTA4LCJJTkNPTUVfU1VNTUEiOjU0NjAzMzguNCwiTlVNIjo2LCJQRVJJT0QiOiIyMDIwLTA0IiwiT1JHTkFNRSI6ItCQ0JrQkSDQotGD0YDQvtC90LHQsNC90LoifSx7Ik9SR19JTk4iOjIwMTA1NTEwOCwiSU5DT01FX1NVTU1BIjo2MDYwMzg0LCJOVU0iOjcsIlBFUklPRCI6IjIwMjAtMDUiLCJPUkdOQU1FIjoi0JDQmtCRINCi0YPRgNC+0L3QsdCw0L3QuiJ9LHsiT1JHX0lOTiI6MjAxMDU1MTA4LCJJTkNPTUVfU1VNTUEiOjYwNjAzNzUuMiwiTlVNIjo4LCJQRVJJT0QiOiIyMDIwLTA2IiwiT1JHTkFNRSI6ItCQ0JrQkSDQotGD0YDQvtC90LHQsNC90LoifSx7Ik9SR19JTk4iOjIwMTA1NTEwOCwiSU5DT01FX1NVTU1BIjoxLjgyMjYxNDY0RTcsIk5VTSI6OSwiUEVSSU9EIjoiMjAyMC0wNyIsIk9SR05BTUUiOiLQkNCa0JEg0KLRg9GA0L7QvdCx0LDQvdC6In0seyJPUkdfSU5OIjoyMDEwNTUxMDgsIklOQ09NRV9TVU1NQSI6ODU3OTEwMi40LCJOVU0iOjEwLCJQRVJJT0QiOiIyMDIwLTA4IiwiT1JHTkFNRSI6ItCQ0JrQkSDQotGD0YDQvtC90LHQsNC90LoifV19LCJpbmNvbWVzX3BlcmlvZCI6eyJpbmNvbWVzX3BlcmlvZF9iZWdpbiI6IjIwMTktMDktMTYiLCJpbmNvbWVzX3BlcmlvZF9lbmQiOiIyMDIwLTA5LTE2IiwiaW5jb21lc19hbGxfc3VtbWEiOjkuNjczNDQ2NjRFN30sInN5c2luZm8iOnsiZGF0ZSI6IjIwMjAtMDktMTYgMTQ6NTI6MDMiLCJiYW5rIjoiXCLQotCj0KDQntCd0JHQkNCd0JpcIiDQkNCiINCR0JDQndCa0JgiLCJpZF9kZW1hbmQiOiIwMTEyMDIwMjYwMzg3NDMzIiwidXNlcl9pZCI6ItC+0YTRhNC70LDQudC9IiwiY2xhaW1faWQiOjEwOTAxMTEsInJlcG9ydF9uYW1lIjoi0KHQstC10LTQtdC90LjRjyDQviDQtNC+0YXQvtC00LDRhSDQv9C+INCY0J3Qn9ChIFhNTCIsImRlY2xhcmF0aW9uIjoi0JLQvdC40LzQsNC90LjQtSEg0J/RgNC10LTQvtGB0YLQsNCy0LvQtdC90LjQtSDQuCDQuNGB0L/QvtC70YzQt9C+0LLQsNC90LjQtSDQutGA0LXQtNC40YLQvdC+0Lkg0LjQvdGE0L7RgNC80LDRhtC40Lgg0YDQtdCz0YPQu9C40YDRg9C10YLRgdGPINCX0LDQutC+0L3QvtC8INCg0LXRgdC/0YPQsdC70LjQutC4INCj0LfQsdC10LrQuNGB0YLQsNC9IFwi0J7QsSDQvtCx0LzQtdC90LUg0LrRgNC10LTQuNGC0L3QvtC5INC40L3RhNC+0YDQvNCw0YbQuNC10LlcIiDihJYgMzAxINC+0YIgMDQuMTAuMjAxMSDQs9C+0LTQsC4iLCJicmFuY2giOiIwMTE0NCIsImNsYWltX2RhdGUiOiIyMDIwLTA5LTE1IiwicmVwb3J0X2NvZGUiOiIwMjUifSwicHJlc2VuY2VfcmVwb3J0cyI6eyJwcmVzZW5jZV9yZXBvcnQiOlt7Im51bSI6MiwicmVwb3J0X25hbWUiOiLQodC60L7RgNC40L3QsyDQmtCY0JDQpiIsInByZXNlbmNlIjoi0JTQsCJ9LHsibnVtIjoxLCJyZXBvcnRfbmFtZSI6ItCY0L3RhNC+0YDQvNCw0YbQuNGPINC/0L4g0LrRgNC10LTQuNGC0LDQvCIsInByZXNlbmNlIjoi0JTQsCJ9XX0sImNsaWVudCI6eyJwaW5mbCI6MzAxMDY5MTE1NzE0MTcsImFkZHJlc3MiOiJKaXp6YXggdmlsb3lhdGkgQmF4bWFsIHR1bWFuaSBJSUIiLCJkb2N1bWVudF9udW1iZXIiOiIwMzEzNjI1IiwiZ2VuZGVyIjoi0JYiLCJkb2N1bWVudF90eXBlX2lkIjo2LCJiaXJ0aF9kYXRlIjoiMTk5Ni0wOS0xNCIsIm5hbWUiOiJYYWxiYXlldmEgT3lndWwgSWJyYWdpbSBRaXppIiwiaW5uIjo1NDQ1Mzc3NjUsImRvY3VtZW50X2RhdGUiOiIyMDE4LTExLTI3IiwicmVzaWRlbnQiOiLQoNC10LfQuNC00LXQvdGCIiwiZG9jdW1lbnRfdHlwZSI6ItCR0LjQvtC80LXRgtGA0LjRh9C10YHQutC40Lkg0L/QsNGB0L/QvtGA0YIg0LPRgNCw0LbQtNCw0L3QuNC90LAg0KDQtdGB0L/Rg9Cx0LvQuNC60Lgg0KPQt9Cx0LXQutC40YHRgtCw0L0iLCJkb2N1bWVudF9zZXJpYWwiOiJBQyJ9LCJub3RpZmljYXRpb25zIjoiIn19';
print_r(base64_decode($base_64)); die;

$data = json_decode($json39, true);
//$m39 = $data['report']['incomes'];
/*$items = array();
foreach($data as $username) {
 $items[] = $username;
}

//print_r($data['report']['incomes']['INCOME']); die;
$fore = $data['report']['incomes']['INCOME']*/
$items = array();
foreach($data as $item) {
	//print_r($item['incomes']['INCOME']); die;
	//$items[] = $item['ORG_INN'];
	$variable = $item['incomes']['INCOME'];
	foreach ($variable as $key => $value) {
		# code...
$inn = $value['ORG_INN'];
$age = $value['INCOME_SUMMA'];
$streetaddress = $value['ORGNAME'];
		   $sql = "INSERT INTO posts(studentId, name, age, streetaddress, city, state, postalcode)
    VALUES('$id', '$inn', '$age', '$streetaddress', '$city', '$state', '$postalcode')";
if(!mysqli_query($mysqli, $sql))
{
    die('Error : ' . mysql_error());
}
	}
	/*print_r($item['incomes']['INCOME'][0]['ORG_INN']); die;
    echo $item['incomes']['INCOME']['ORG_INN'];
    //echo $item['ORG_INN'];

    // to know what's in $item
    echo '<pre>'; var_dump($item);*/
    


 


}


//get the student details
$id = $data['html']['studentId'];
$name = $data['html']['personal']['name'];
$age = $data['html']['personal']['age'];
$streetaddress = $data['html']['personal']['address']['streetaddress'];
$city = $data['html']['personal']['address']['city'];
$state = $data['html']['personal']['address']['state'];
$postalcode = $data['html']['personal']['address']['postalcode'];
//print_r($m39); die;
//insert into mysql table
$sql = "INSERT INTO posts(studentId, name, age, streetaddress, city, state, postalcode)
    VALUES('$id', '$name', '$age', '$streetaddress', '$city', '$state', '$postalcode')";
if(!mysqli_query($mysqli, $sql))
{
    die('Error : ' . mysql_error());
}

?>
